gText_Birch_Welcome::
	.string "¡Hola! ¡Siento llegar tarde!\p"
	.string "¡Bienvenide al mundo POKéMON!\p"
	.string "Mi nombre es PAU.\p"
	.string "Pero todo el mundo me llama PROF.\n"
	.string "R2D2.\p"
	.string "$"

gText_Birch_Pokemon::
	.string "Esto de aquí es un “POKéMON.”\p"
	.string "\n"
	.string "$"

gText_Birch_MainSpeech::
	.string "Madre mía, ¡un LUDICOLO shiny!\p"
	.string "No vas a volver a ver uno\n"
	.string "así en tu vida.\p"
	.string "Ese es mi regalo, feliz cumpleaños.\p"
	.string "… … … … … … … … \n"
	.string "… … … … … … … … \l"
	.string "… … … … … … … … \l"
	.string "… … … … … … … … \p"
	.string "No hombre no, ¿te imaginas?\n"
	.string "Ya me jodería.\p"
	.string "Quería poner un BLAZIKEN,\n"
	.string "pero el shiny es indistinguible.\p"
	.string "Y encima FUEGO/LUCHA, vaya\n"
	.string "despropósito de POKéMON.\p"
	.string "Bueno, en qué estaba...\n"
	.string "Ah sí, ¡POKéMON!\p"
	.string "Están bien y tal.\p"
	.string "Lo que también está muy\n"
	.string "bien son los regalos.\p"
	.string "He montado esto para\n"
	.string "enseñarte tu regalo.\p"
	.string "$"

gText_Birch_AndYouAre::
	.string "Pero primero habrá que\n"
	.string "darte un avatar, para\l"
	.string "que el juego no se queje.\n"	
	.string "$"

gText_Birch_BoyOrGirl::
	.string "¿Cuál prefieres?$"

gText_Birch_WhatsYourName::
	.string "Muy bien.\n"
	.string "¿Qué nombre ponemos?$"

gText_Birch_SoItsPlayer::
	.string "¿Vale, o sea que {PLAYER}?$"

gText_Birch_YourePlayer::
	.string "¡{PLAYER}! ¡Vaya nombre!\p"
	.string "¡Como el mono!\n"
	.string "(O no, es difícil de predecir)\p"
	.string "$"

gText_Birch_AreYouReady::
	.string "Vale, pues ya está.\p"
	.string "Ahora empieza tu viaje\n"
	.string "de búsqueda de tu regalo.\p"
	.string "Pero antes te quiero pedir\n"
	.string "una cosa.\p"
	.string "He puesto detallitos,\n"
	.string "especialmente diálogos, en\p"
	.string "este mundo. Explóralo bien,\n"
	.string "que me he esforzado mucho.\p"
	.string "Adelante, ¡empieza tu aventura!\p"
	.string "$"
